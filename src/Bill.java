import java.util.ArrayList;
import java.util.List;

public class Bill {
    public static ChequingAccount megaBank = new ChequingAccount(new MoneyType(MoneyType.USD, 0), "Bank of Canada", 1e6);
    private int billNum;
    private String billName;
    private MoneyType owed;
    private ChequingAccount payee;

    /**Constructor for the Bill object
     *	@param		billName		name of the bill
     *  @param		billNum			number of the bill
     *	@param      owed            the amount owed
     *  @param      payee           the payee of the bill
     */
    public Bill(String billName, int billNum, MoneyType owed, ChequingAccount payee) {
        this.billNum = billNum;
        this.billName = billName;
        this.owed = owed;
        this.payee = payee;
    }

    /**
     * Generates a number of bills with fairly random parameters
     *
     * @param numberOfBills
     * @param umbrellaAccount holds the account of all the companies
     * @return a list of bills
     */
    public static List<Bill> getRandomBills(int numberOfBills, ChequingAccount umbrellaAccount) {
        List<Bill> bills = new ArrayList<Bill>();
        double companyRandomizer = Math.random();
        for (int i = 0; i < numberOfBills; ++i, companyRandomizer = Math.random()) {
            if (companyRandomizer < 0.2) {
                bills.add(new Bill("Amazon.co.uk", i, new MoneyType(MoneyType.EUR, Math.random() * 200), umbrellaAccount));
            } else if (companyRandomizer < 0.4) {
                bills.add(new Bill("Amazon.co.jp", i, new MoneyType(MoneyType.JPY, Math.random() * 17000), umbrellaAccount));
            } else if (companyRandomizer < 0.6) {
                bills.add(new Bill("Amazon.com", i, new MoneyType(MoneyType.USD, Math.random() * 200), umbrellaAccount));
            } else if (companyRandomizer < 0.8) {
                bills.add(new Bill("Harvard Tuition", i, new MoneyType(MoneyType.USD, 10000 + 20000 * Math.random()), umbrellaAccount));
            } else {
                bills.add(new Bill("Credit Card Bill", i, new MoneyType(MoneyType.USD, Math.random() * 1000), umbrellaAccount));
            }
        }
        return bills;
    }

    /**    Pay bill method, pays the entire bill
     *	@param	payer	the payer of the bill
     *	@return	success	of payment
     */
    public boolean payBillFull(BankAccount payer) {
        //return the transfer code - true for success, false for insufficient funds or past limit
        if (payer.transfer(payee, owed)) {
            owed.sub(owed);
            return true;
        }
        return false;
    }

    /**
     * Pay bill method, pays the amount provided in the parameter
     *
     * @param payer  the payer of the bill
     * @param amount the amount to be paid
     * @return success    of payment
     */
    public boolean payBill(BankAccount payer, MoneyType amount) {
        //return the transfer code - true for success, false for insufficient funds or past limit
        if (payer.transfer(payee, amount)) {
            owed.sub(amount);
            return true;
        }
        return false;
    }

    /**    Getter method for name
     *	@return billName
     */
    public String getName() {
        return billName;
    }

    /**    Getter method for the bill number
     *	@return billNum
	 */
    public int getNumber() {
        return billNum;
    }

    /**    Getter method for amount
     *	@return the cost
     */
    public String getAmount() {
        return owed.toString();
    }

    public boolean paidInFull() {
        return owed.empty();
    }

    /**  Method to retrieve the payee account
     *  @return payee account
     */
    public ChequingAccount getPayee() {
        return payee;
    }

    /**    @override
     *	Get the bill as a string
     *	@return the bill formatted as a string
     */
    public String toString() {
        return "Bill #" + billNum + ": " + owed + " owed to " + billName;
    }
}