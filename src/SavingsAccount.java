
/**
 * Created by Darren on 3/30/2016.
 */
public class SavingsAccount extends BankAccount {
    private double interestRate;

    /**
     * Savings account constructor
     *
     * @param balance
     * @param name
     * @param interestRate
     */
    public SavingsAccount(MoneyType balance, String name, double interestRate) {
        super(balance, name);
        this.interestRate = interestRate;
    }

    /*  Withdraw money from the account
     *  @param  amount
     *  @return success of withdrawal
     */
    public boolean withdraw(MoneyType amount) {
        if (sufficientBalance(amount) && !locked) {
            balance.sub(amount);
            return true;
        }
        return false;
    }

    /**  @override
     *  Overriden toString method
     *  @return formatted SavingsAccount string
     */
    public String toString() {
        return "[Savings] Account carries a balance of " + balance
                + " with an interest rate of " + interestRate + "%";
    }

    /** Compare accounts
     *  NOTE that you can just use == instead
     *  in this case
     *
     *  @param  other account
     *  @return if they are the same
     */
    public boolean equals(BankAccount other){
        return (other instanceof SavingsAccount);
    }
}
