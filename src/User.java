import java.util.ArrayList;
import java.util.List;

public class User {

    //Attempt to retrofit the proper architecture onto user.java
    //The user class here will generate everything needed per user
    private BankCard bankCard;
    private String name;


    private long cardNumber;
    private int pin;
    private BankAccount savings;
    private BankAccount chequing;
    private List<Bill> bills;
    private List<Transaction> transactions;

    /**
     * Constructor for a user
     *
     * @param cardNum
     * @param pin
     * @param savings
     * @param chequing
     * @param bills
     */
    public User(long cardNum, int pin, BankAccount savings, BankAccount chequing, List<Bill> bills) {
        cardNumber = cardNum;
        this.pin = pin;
        this.savings = savings;
        this.chequing = chequing;
        this.bills = bills;
        transactions = new ArrayList<Transaction>();
    }

    /**
     * New constructor for a user
     *
     * @param name
     * @param cardNum
     * @param pin
     */
    public User(String name, long cardNum, int pin) {
        String bankName;
        if (cardNum % 5 == 0) bankName = "RBC";
        else if (cardNum % 5 == 1) bankName = "ScotiaBank";
        else if (cardNum % 5 == 2) bankName = "TD";
        else if (cardNum % 5 == 3) bankName = "BMO";
        else/* (cardNum % 5 == 4)*/bankName = "CIBC";
        this.bankCard = new BankCard(bankName, name, cardNum, pin);
    }

    /**    Create a list of users
     *	For development purposes only
     *	@return	a list of users
     */
    public static List<User> getUserList() {
        List<User> users = new ArrayList<User>();
        List<Bill> bills;
        ChequingAccount chequing;
        SavingsAccount savings;
        User user;
        String name;

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "DARREN MEI";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 1, 1111);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "ERIC JASKIERSKI";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 2, 2222);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "KENNY NGUYEN";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 3, 3333);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "EMMANANUEL SINLAO";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 4, 4444);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "JOSHUA COLACO";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 5, 5555);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "ZULFIKAR YUSUFALI";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6), name, 5.00);
        user = new User(name, 6, 6666);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        users.add(user);

        return users;
    }

    /**
     * Static method to create a user
     *
     * @param cardNum
     * @param pin
     * @return user
     */
    public static User createUser(int cardNum, int pin) {
        List<Bill> bills;
        List<Transaction> transactions;
        ChequingAccount chequing;
        SavingsAccount savings;
        User user;
        String name;

        bills = Bill.getRandomBills(10, Bill.megaBank);
        name = "TEMPORARY";
        chequing = new ChequingAccount(new MoneyType(MoneyType.USD, 0), name, 1000.00);
        savings = new SavingsAccount(new MoneyType(MoneyType.USD, 1e6 * Math.random()), name, 5.00);
        user = new User(name, cardNum, pin);
        user.bankCard.setChequingAccount(chequing);
        user.bankCard.setSavingsAccount(savings);
        user.bills = bills;
        user.transactions = new ArrayList<Transaction>();
        return user;
    }

	/*
    public static User createUser(int cardNum, int pin){
		Random random = new Random();
		int var = 0;
		
		ArrayList<Bill> bills = new ArrayList<Bill>();
		Account savingsAccount = new Account("Savings",MoneyType.USD, random.nextInt(100000) + random.nextDouble(), var++);
		Account chequingAccount = new Account("Chequing",MoneyType.USD, random.nextInt(10000) + random.nextDouble(), var++);
		int dummy = random.nextInt(5);
		for(int i = 0; i < dummy; i++){
			Bill bill = new Bill("Payee Company", i+1, new MoneyType(MoneyType.USD, random.nextInt(500) + Double.parseDouble(new DecimalFormat("#.##").format(random.nextDouble()))));
			bills.add(bill);
		}
		User user = new User(cardNum, pin, savingsAccount, chequingAccount, bills);
		return user;
	}
	*/
	/*
	public void payBill(Account account, Bill bill){
		if(bill.payBill(chequing,account))
			bills.remove(bill);
	}
	*/

    /**
     * Pay a bill
     * @param account
     * @param bill
     */
    public void payBillFull(BankAccount account, Bill bill) {
        if (bill.payBillFull(account))
            bills.remove(bill);
    }

    public void payBill(BankAccount account, Bill bill, MoneyType amount) {
        if (bill.payBill(account, amount))
            if (bill.paidInFull())
                bills.remove(bill);
    }

    /**
     * Add a transaction
     * @param bankAccount
     * @param type
     * @param amount
     */
    public void addTransaction(BankAccount bankAccount, byte type, MoneyType amount) {
        transactions.add(new Transaction(bankAccount, type, amount));
    }

    /**
     * get a chequing account
     * @return chequingAccount
     */
    public ChequingAccount getChequingAccount() {
        return bankCard.getChequingAccount();
    }

    /**
     * get a savings account
     * @return savingsAccount
     */
    public SavingsAccount getSavingsAccount() {
        return bankCard.getSavingsAccount();
    }

    /**
     * get the list of bills
     * @return list of bills
     */
    public List<Bill> getBills() {
        return bills;
    }

    /**
     * get the list of transactions
     * @return list of transactions
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * get the bank card assigned to the user
     *
     * @return
     */
    public BankCard getBankCard() {
        return bankCard;
    }

    public long getId() {
        return cardNumber;
    }
}