/**
 * Created by Darren on 3/30/2016.
 */
abstract class BankAccount {
    protected MoneyType balance;
    protected String name;
    //for usage of the report fraud use case
    protected boolean locked;
    //protected String accountType;

    /**  BankAccount abstract constructor
     *  @param  balance
     *  @param  name
     */
    public BankAccount(MoneyType balance, String name) {
        //this.accountType = accountType;
        this.balance = balance;
        this.name = name;
        this.locked = false;
    }

    /**    Deposit money into the account
     *	@param	amount	the amount of money to deposit
	 */
    public void deposit(MoneyType amount) {
        balance.add(amount);
    }

    /** Check if the bank account has a sufficient balance
     *  @param  amount
     *  @return if sufficient balance or not
     */
    public boolean sufficientBalance(MoneyType amount) {
        return balance.ge(amount);
    }

    /** Get the balance
     *  @return balance
     */
    public MoneyType getBalance() {
        return balance;
    }

    /** Transfer from one bank account into another
     *  @param  toAccount
     *  @param  amount
     *  @return success of transfer
     */
    public boolean transfer(BankAccount toAccount, MoneyType amount) {
        //if it is possible to withdraw
        if (sufficientBalance(amount) && !locked) {
            toAccount.deposit(amount);
            return true;
        }
        //return false on error
        return false;
    }

    /**
     * Retrieve the owner name
     *
     * @return
     */
    public String getName(){
        return name;
    }

    /**
     * Method for locking the account
     * @param lockEnable
     */
    public void lockAccount(boolean lockEnable) {
        this.locked = lockEnable;
    }

    /**
     * Check if this bankaccount is equal to the other one
     *
     * @param other
     * @return ==
     */
    abstract public boolean equals(BankAccount other);

    /** Withdraw money from the account
     *  @param  amount
     *  @return success of withdrawal
     */
    abstract public boolean withdraw(MoneyType amount);

    /** @override
     *  Overriden toString method
     *  @return formatted BankAccount string
     */
    abstract public String toString();



}
