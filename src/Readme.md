Darren: DO NOT PUSH ONTO THIS BRANCH, ONCE THIS BRANCH IS DONE WITH THE CHANGES, IT WILL BE PUSHED ONTO DEV

The list of completed use cases:

1.Deposit_Fund				

2.Display_Fund				

3.Login

4.Pay_Bill					

5.Transaction				  

6.Transfer_Funds			

7.Withdraw_Funds			

Partially completed use cases:

1. Convert_Currency 		

2. Set_Limits           -- wip

3. FAQs 					      

4. Recent_Transaction_Report - initial support

5. Report_Fraud         -- wip

Todo:

1.Check_Bill_Aut			-- how is this possible

2.Print_Account_Balance 	-- Implement by making a receipt or passbook class?
