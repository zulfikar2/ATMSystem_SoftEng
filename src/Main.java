import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main{
	public static void main(String args[]){
		ATM atm = new ATM();
		Session session = new Session(atm, null);
		
		Frame mainFrame = new Frame("ATM");
		mainFrame.add(session.getGUI());
		
		mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            }
        });
		
		new Thread(session).start();
		
		mainFrame.setResizable(false);
        mainFrame.pack();
        mainFrame.setVisible(true);
		mainFrame.setSize(1000, 400);
	}
}

