import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darren on 3/30/2016.
 */
public class Question {
    String q, a;

    /*  Constructor for the question class
     *  @param  q   the question
     *  @param  a   the answer
     */
    public Question(String q, String a) {
        this.q = q;
        this.a = a;
    }

    /*  Provide the FAQs list in order to implement FAQs use case
     *  @return list of questions
     */
    public static List<Question> getDefaultQuestionList(){
        List<Question> questions = new ArrayList<Question>();
        String question,answer;

        question = "What is the withdrawal limit at this ATM?";
        answer = "It is set by your bank provider. Default is 1000.00 in your current currency.";
        questions.add(new Question(question, answer));

        question = "How do I change my withdrawal limit on this ATM?";
        answer = "This feature is available as soon as it is implemented.";
        questions.add(new Question(question, answer));

        question = "What is the interest rate on the savings account?";
        answer = "The interest rate on the savings account is 5.0%.";
        questions.add(new Question(question, answer));

        question = "How do I log out?";
        answer = "This feature is available as soon as it is implemented.";
        questions.add(new Question(question, answer));

        return questions;
    }

    /*  @override
     *  @return the question in a formatted way
     */
    public String toString() {
        return "Question:\n" + q + "\n" + "Answer:" + a + "\n\n";
    }
}
