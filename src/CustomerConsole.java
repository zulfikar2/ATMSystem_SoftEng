public class CustomerConsole
{
    /** Constructor
     */
    public CustomerConsole()
    {
    }
    
    /** Display a message to the customer
     *
     *  @param message the message to display
     */
    public void display(String message)
    {
        Session.getSession().clearDisplay();
        Session.getSession().display(message);
    }

    
    /** Display a menu of options and return choice made by customer
     *
     *  @param prompt message to display before the list of options
     *  @param menu the options
     *  @return the number of the option chosen (0 .. # of options - 1)
     *  Note: the options are numbered 1 .. # of options when displayed for the
     *  customer - but the menu array indices and the final result returned are in
     *  the range 0 .. # of options - 1
     *
     *  @exception Cancelled if customer presses the CANCEL key before choosing option
     */
    public synchronized int readMenuChoice(String prompt, String[] menu) throws Cancelled
    {
        Session.getSession().clearDisplay();
        Session.getSession().display(prompt);
        for (int i = 0; i < menu.length; i ++)
            Session.getSession().display((i+1) + ") " + menu[i]);

        String input = Session.getSession().readInput(Session.MENU_MODE);
            
        Session.getSession().clearDisplay();
		System.out.println(input);
        if (input == null || input == "")
			throw new Cancelled();
        else
			return Integer.parseInt(input);
    }


    /** Read a money amount entered by the customer
     *
     *  @param prompt the message to display prompting the customer to enter amount
     *  @return the amount entered by the customer
     *  @exception Cancelled if customer presses the CANCEL key before pressing ENTER
     */
    public synchronized double readAmount(String prompt) throws Cancelled
    {
        Session.getSession().clearDisplay();
        Session.getSession().display(prompt);
        Session.getSession().display("");
        
        String input = Session.getSession().readInput(Session.AMOUNT_MODE);
        
        Session.getSession().clearDisplay();
        
        if (input == null)
           throw new Cancelled();
        else
        {
            double dollars = Double.parseDouble(input);
            //int cents = Integer.parseInt(input) % 100;
            //return new MoneyType(dollars, cents);
			System.out.println(input);
			return dollars;
        }
    }

    /** Exception thrown when the user presses the cancel key while the ATM is
     *  waiting for some action
     */
    public static class Cancelled extends Exception
    {
        /** Constructor
         */
        public Cancelled()
        {
            super("Cancelled by customer");
        }
    }
}