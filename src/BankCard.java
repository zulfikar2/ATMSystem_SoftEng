/**
 * Created by Darren on 3/30/2016.
 */
public class BankCard {
    protected String bankType;
    protected String cardOwner;
    protected long cardNum;
    private int cardPin;
    private ChequingAccount chequingAccount;
    private SavingsAccount savingsAccount;

    /** Constructor for BankCard
     *  @param  bankType
     *  @param  cardOwner
     *  @param  cardPin
     */
    public BankCard(String bankType, String cardOwner, long cardNum, int cardPin){
        this.bankType = bankType;
        this.cardOwner = cardOwner;
        this.cardNum = cardNum;
        this.cardPin = cardPin;
    }

    /**
     * Makes a new bankcard with only card number defined
     *
     * @param cardNum
     */
    public BankCard(long cardNum) {
        this.bankType = "Bank of Toronto";
        this.cardOwner = "ISSAC NEWTON";
        this.cardNum = cardNum;
        this.cardPin = -1;
    }

    /** Gets the chequing account
     *  @return  chequing account
     */
    public ChequingAccount getChequingAccount(){
        return chequingAccount;
    }

    /**
     * Sets the chequingAccount
     *
     * @param chequingAccount
     */
    public void setChequingAccount(ChequingAccount chequingAccount) {
        this.chequingAccount = chequingAccount;
    }

    /** Gets the savings account
     *  @return  savings account
     */
    public SavingsAccount getSavingsAccount(){
        return savingsAccount;
    }

    /**
     * Sets the savings account
     *
     * @param savingsAccount
     */
    public void setSavingsAccount(SavingsAccount savingsAccount) {
        this.savingsAccount = savingsAccount;
    }

    /**
     * get card number
     *
     * @return cardnum
     */
    public long getCardId() {
        return cardNum;
    }

    /**
     * set the pin of the card
     *
     * @param cardPin
     */
    public void setPin(int cardPin) {
        this.cardPin = cardPin;
    }

    /** Check if the pins are identical
     *  @param pin
     *  @return result
     */
    public boolean compareTo(int pin){
        return cardPin == pin;
    }


    /**  @override
     *  prints the bankcard
     *  @return formatted BankCard string
     */
    public String toString(){
        return "[" + bankType + "] " + cardOwner + "\nCard number :" + cardNum;
    }

}
