import java.util.List;

/**
 * Created by Darren on 3/30/2016.
 *
 *
 * To be integrated into the session class
 *
 *
 */
public class ATMSession {
    public static final int MAX_ATTEMPTS = 3;

    private User user;
    private long userID;
    private int attempts;
    private boolean verified;

    /**
     * Constructor for the ATMSession
     */
    public ATMSession() {
        user = null;
        this.userID = 0;
        attempts = 0;
        verified = false;
    }

    /**
     * set the userID
     *
     * @param userID
     */
    public void setUserID(long userID) {
        this.userID = userID;
    }

    /**
     * Finds the card info from the list
     * if not found, makes a new card
     */
    public User findUserInfo(int pin, List<User> userList) {
        for (User userInstance : userList) {
            if (userInstance.getId() == userID) {
                return userInstance;
            }
        }
        //bank info not found, make a new account with no pin
        User temp = new User("John Smith", userID, pin);
        userList.add(temp);
        return temp;
    }

    /**
     * Check if the stored info match
     *
     * @param pin
     * @param userList
     * @return verified login attempt
     */
    public boolean login(int pin, List<User> userList) {
        if (user == null) {
            user = findUserInfo(pin, userList);
        }
        if(!verified && attempts < MAX_ATTEMPTS) {
            if (user.getBankCard().compareTo(pin))
                verified = true;
            ++attempts;
        }
        return verified;
    }

    /**
     * Check if the stored info match
     *
     * @param userID
     * @param pin
     * @param userList
     * @return verified login attempt
     */
    public boolean login(long userID, int pin, List<User> userList) {
        setUserID(userID);
        if (user == null) {
            user = findUserInfo(pin, userList);
        }
        if (!verified && attempts < MAX_ATTEMPTS) {
            if (user.getBankCard().compareTo(pin))
                verified = true;
            ++attempts;
        }
        return verified;
    }

    public User fetchUser(){
        if(verified)
            return user;
        return null;
    }

}
