import java.util.List;

public class ATM {
    private static final String[] FROM_ACCOUNT =
            {"From Chequing", "From Savings"};
    private static final String[] TO_ACCOUNT =
            {"To Chequing", "To Savings"};

    private final int FROM = 1;
    private final int TO = 2;
    private final int CHEQUING = 1;
    private final int SAVINGS = 2;
    private final int CANCEL = 3;
    private String WITHDRAW = "Withdraw";
    private String DEPOSIT = "Deposit";
    private String TRANSFER = "Transfer";
    private String BILL_PAYMENT = "Bill Payment";
    private CustomerConsole customerConsole;
    private byte currentCurrency = MoneyType.USD;

    public ATM() {
        customerConsole = new CustomerConsole();
    }

    public CustomerConsole getCustomerConsole() {
        return customerConsole;
    }

    public int chooseAccount(int i) {
        int acc = 121212;
        try {
            if (i == FROM)
                acc = this.getCustomerConsole().readMenuChoice("Please choose your account", FROM_ACCOUNT);
            else
                acc = this.getCustomerConsole().readMenuChoice("Please choose your account", TO_ACCOUNT);
        } catch (CustomerConsole.Cancelled e) {
            acc = CANCEL;
        }

        return acc;
    }

    public BankAccount getAccount(User user, int i) {
        if (i == CHEQUING)
            return user.getChequingAccount();
        else
            return user.getSavingsAccount();
    }

    public void withdraw(User user) {
        int usersChoice = chooseAccount(FROM);

        if (usersChoice != CANCEL) {
            BankAccount account = getAccount(user, usersChoice);
            try {
                double amount = this.getCustomerConsole().readAmount("Enter the amount that you would like to withdraw");

                boolean valid = account.withdraw(new MoneyType(currentCurrency, amount));
                if (!valid)
                    this.getCustomerConsole().display("FAILED");//System.out.println("Failed to withdraw");
                else {
                    user.addTransaction(account, Transaction.WITHDRAW, new MoneyType(currentCurrency, amount));
                    this.getCustomerConsole().display("FAILED");
                }
            } catch (CustomerConsole.Cancelled e) {
            }
        }
    }

    public void deposit(User user) {
        int usersChoice = chooseAccount(TO);

        if (usersChoice != CANCEL) {
            BankAccount account = getAccount(user, usersChoice);
            try {
                double amount = this.getCustomerConsole().readAmount("Enter the amount that you would like to deposit");

                account.deposit(new MoneyType(currentCurrency, amount));
                user.addTransaction(account, Transaction.DEPOSIT, new MoneyType(currentCurrency, amount));
            } catch (CustomerConsole.Cancelled e) {
            }
        }
    }

    public void transfer(User user) {
        int usersChoice = chooseAccount(FROM);

        if (usersChoice != CANCEL) {
            BankAccount fromAccount = getAccount(user, usersChoice);
            int usersSecondChoice = chooseAccount(TO);

            if (usersSecondChoice != CANCEL) {
                BankAccount toAccount = getAccount(user, usersSecondChoice);
                if (fromAccount.equals(toAccount))
                    System.out.println("ERROR");
                else {
                    try {
                        double amount = this.getCustomerConsole().readAmount("Enter the amount that you would like to withdraw");
                        //We could add an if statement here to check if the transfer succeeded or not.
                        //if(fromAccount.transfer(...)
                        //      then user.addTransaction(...)
                        //else
                        //      print out insufficient balance error
                        fromAccount.transfer(toAccount, new MoneyType(currentCurrency, amount));
                        user.addTransaction(fromAccount, Transaction.TRANSFER, new MoneyType(currentCurrency, amount));
                    } catch (CustomerConsole.Cancelled e) {
                    }
                }
            }
        }
    }

    public synchronized void balanceInquiry(User user) {
        int usersChoice = chooseAccount(FROM);
        BankAccount account = getAccount(user, usersChoice);
        if (usersChoice != CANCEL) {
            this.getCustomerConsole().display("Your account balance is: \n" + account.getBalance().convert(currentCurrency));
            Session.getSession().getKeyboard().waitForKeyPress();
        }
    }

    public void billPayment(User user) {
        int usersChoice = chooseAccount(FROM);

        if (usersChoice != CANCEL) {
            List<Bill> bills = user.getBills();
            if (!bills.isEmpty()) {
                BankAccount account = getAccount(user, usersChoice);
                String bs[] = new String[bills.size()];
                for (int i = 0; i < bills.size(); i++) {
                    Bill bill = bills.get(i);
                    bs[i] = bill.getName() + "00" + bill.getNumber() + "\n                        " + bill.getAmount();
                }

                try {
                    int input = this.getCustomerConsole().readMenuChoice("Please choose bill", bs);
                    if (input != bills.size() + 1) {
                        //System.out.println("How much do you want to pay");
                        currentCurrency = bills.get(input - 1).getPayee().getBalance().getCurrency();
                        double amount = this.getCustomerConsole().readAmount("How much do you want to pay");
                        //double amount = 1.1;//reader.nextDouble();

                        //We could add an if statement here to check if the bill payment succeeded or not.
                        //if(user.payBill(...)
                        //      then user.addTransaction(...)
                        //else
                        //      print out insufficient balance error
                        user.payBill(account, bills.get(input - 1),
                                new MoneyType(bills.get(input - 1).getPayee().getBalance().getCurrency(), amount));
                        user.addTransaction(account, Transaction.PAYMENT,
                                new MoneyType(bills.get(input - 1).getPayee().getBalance().getCurrency(), amount));
                    }
                } catch (CustomerConsole.Cancelled e) {
                }
            } else {
                this.getCustomerConsole().display("You have no bills to pay");
                Session.getSession().getKeyboard().waitForKeyPress();
            }
        }
    }

    public void recentTransactions(User user) {
        List<Transaction> transactions = user.getTransactions();
        if (transactions.isEmpty()) {
            this.getCustomerConsole().display("You have no recent transactions");
            //System.out.println("You have no recent transactions");
            Session.getSession().getKeyboard().waitForKeyPress();
        } else {
            String listOfTransString = "Recent Transactions:\n";
            //System.out.println("Recent Transactions:");
            for (int i = 0; i < transactions.size(); i++) {
                Transaction trans = transactions.get(i);
                //System.out.printf("#%d %s\n", i+1, trans);
                listOfTransString += "#" + Integer.toString(i + 1) + " " + trans + "\n";
            }
            this.getCustomerConsole().display(listOfTransString);
            Session.getSession().getKeyboard().waitForKeyPress();
        }
    }

    /**
     * Set the currency displayed on the ATM
     *
     * @param currentCurrency
     */
    public void setCurrentCurrency(byte currentCurrency) {
        this.currentCurrency = currentCurrency;
    }
}