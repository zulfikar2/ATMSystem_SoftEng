import java.awt.*;

public class ATMPanel extends Panel {
    public static final int DISPLAYABLE_LINES = 9;
    public static final String BLANK_DISPLAY_LINE =
            "                                             ";
    public static final int PRINTABLE_LINES = 9;
    public static final int PRINTABLE_CHARS = 30;

    // The following are used only for doing the layout

    private static final int DISPLAY_ROW = 0;
    private static final int DISPLAY_COL = 0;
    private static final int DISPLAY_WIDTH = 3;
    private static final int DISPLAY_HEIGHT = 1;
    private static final int DISPLAY_FILL = GridBagConstraints.BOTH;

    private static final int PRINTER_ROW = 0;
    private static final int PRINTER_COL = DISPLAY_COL + DISPLAY_WIDTH;
    private static final int PRINTER_WIDTH = 1;
    private static final int PRINTER_HEIGHT = 1;
    private static final int PRINTER_FILL = GridBagConstraints.NONE;

    private static final int ENVELOPE_ROW = DISPLAY_ROW + DISPLAY_HEIGHT;
    private static final int ENVELOPE_COL = 0;
    private static final int ENVELOPE_WIDTH = 1;
    private static final int ENVELOPE_HEIGHT = 1;
    private static final int ENVELOPE_FILL = GridBagConstraints.NONE;

    private static final int DISPENSER_ROW = ENVELOPE_ROW;
    private static final int DISPENSER_COL = ENVELOPE_COL + ENVELOPE_WIDTH;
    private static final int DISPENSER_WIDTH = 1;
    private static final int DISPENSER_HEIGHT = 1;
    private static final int DISPENSER_FILL = GridBagConstraints.NONE;

    private static final int READER_ROW = ENVELOPE_ROW;
    private static final int READER_COL = DISPENSER_COL + DISPENSER_WIDTH;
    private static final int READER_WIDTH = 1;
    private static final int READER_HEIGHT = 1;
    private static final int READER_FILL = GridBagConstraints.NONE;

    private static final int KEYBOARD_ROW = ENVELOPE_ROW;
    private static final int KEYBOARD_COL = READER_COL + READER_WIDTH;
    private static final int KEYBOARD_WIDTH = 1;
    private static final int KEYBOARD_HEIGHT = 1;
    private static final int KEYBOARD_FILL = GridBagConstraints.NONE;

    private static final int SHOW_LOG_BUTTON_ROW = ENVELOPE_ROW + ENVELOPE_HEIGHT;
    private static final int SHOW_LOG_BUTTON_COL = 0;
    private static final int SHOW_LOG_BUTTON_WIDTH = 1;
    private static final int SHOW_LOG_BUTTON_HEIGHT = 1;
    private static final int SHOW_LOG_BUTTON_FILL = GridBagConstraints.BOTH;

    private static final int OPERATOR_ROW = SHOW_LOG_BUTTON_ROW;
    private static final int OPERATOR_COL = SHOW_LOG_BUTTON_COL + SHOW_LOG_BUTTON_WIDTH;
    private static final int OPERATOR_WIDTH = 3;
    private static final int OPERATOR_HEIGHT = 1;
    private static final int OPERATOR_FILL = GridBagConstraints.BOTH;

    private static final int TOTAL_ROWS = 3;
    private static final int TOTAL_COLS = 3;

    public ATMPanel(final GUI gui, Display display, Keyboard keyboard) {
        GridBagLayout atmLayout = new GridBagLayout();
        setLayout(atmLayout);

        add(display);
        atmLayout.setConstraints(display, GUI.makeConstraints(DISPLAY_ROW, DISPLAY_COL, DISPLAY_WIDTH, DISPLAY_HEIGHT, DISPLAY_FILL));

        add(keyboard);
        atmLayout.setConstraints(keyboard, GUI.makeConstraints(PRINTER_ROW, KEYBOARD_COL, KEYBOARD_WIDTH, KEYBOARD_HEIGHT, KEYBOARD_FILL));
    }
}