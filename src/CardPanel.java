import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CardPanel extends Panel {
    private TextField cardNumberField;
    private TextField pinNumberField;

    public CardPanel() {
        setLayout(new GridLayout(0, 1, 0, 0));
        setFont(new Font("Monospaced", Font.PLAIN, 14));
        add(new Label("A real ATM would have a magnetic", Label.CENTER));
        add(new Label("stripe reader to read the card", Label.CENTER));
        add(new Label("For purposes of the simulation,", Label.CENTER));
        add(new Label("please enter the card and pin number manually.", Label.CENTER));
        add(new Label("Then press RETURN", Label.CENTER));
        add(new Label("(The card number must be a positive integer", Label.CENTER));
        add(new Label("and the pin must be four digits long)", Label.CENTER));

        List<User> userList = User.getUserList();
        ATMSession atmSession = new ATMSession();

        cardNumberField = new TextField(30);
        pinNumberField = new TextField(30);
        pinNumberField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkCard(atmSession, userList);
            }
        });
        Panel cardNumberPanel = new Panel();
        Label cardNumberLable = new Label("Card Number: ");
        cardNumberPanel.add(cardNumberLable);
        cardNumberPanel.add(cardNumberField);

        Panel pinNumberPanel = new Panel();
        Label pinNumberLabel = new Label("Pin:         ");
        pinNumberPanel.add(pinNumberLabel);
        pinNumberPanel.add(pinNumberField);

        add(cardNumberPanel);
        add(pinNumberPanel);
    }

    public synchronized void checkCard(ATMSession atmSession, List<User> userList) {
        try {
            int cardNum = Integer.parseInt(cardNumberField.getText());
            int pin = Integer.parseInt(pinNumberField.getText());
            if (cardNum <= 0 || pin < 1000 || pin > 9999)
                System.out.println("Incorrect pin");

            else {
                if (atmSession.login(cardNum, pin, userList)) {
                    Session.getSession().getGUI().showATM();
                    Session.getSession().newUser(User.createUser(cardNum, pin));
                }
                //Session.getSession().performSession();
            }
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }
}