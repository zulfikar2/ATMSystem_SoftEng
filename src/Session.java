public class Session implements Runnable
{
    private int pin;
    private int state;

    private static final int READING_CARD_STATE = 1;
    private static final int READING_PIN_STATE = 2;
    private static final int CHOOSING_TRANSACTION_STATE = 3;
    private static final int PERFORMING_TRANSACTION_STATE = 4;
    private static final int EJECTING_CARD_STATE = 5;
    private static final int FINAL_STATE = 6;
	
	private static final String [] TRANSACTION_TYPES_MENU = 
        { "Withdrawal", "Deposit", "Transfer", "Balance Inquiry", "Bill Payment", "Recent Transactions", "FAQs" };
	
	/* Possible values for mode parameter to readInput() */
    
    /** Read input in PIN mode - allow user to enter several characters,
     *  and to clear the line if the user wishes; echo as asterisks
     */
    public static final int PIN_MODE = 1;
    
    /** Read input in amount mode - allow user to enter several characters,
     *  and to clear the line if the user wishes; echo what use types
     */
    public static final int AMOUNT_MODE = 2;
    
    /** Read input in menu choice mode - wait for one digit key to be pressed,
     *  and return value immediately.
     */
    public static final int MENU_MODE = 3;
	
	private ATM atm;
	private User user;
	private Display display;
	private Keyboard keyboard;
	private GUI gui;
	
	private static Session theSession;
	
    public Session(ATM atm, User user)
    {
		this.user = user;
        this.atm = atm;
		display = new Display();
		keyboard = new Keyboard(display);
		gui = new GUI(display, keyboard);
        
        state = READING_CARD_STATE;
		
		theSession = this;
    }
	
	public void newUser(User user){
		this.user = user;
	}
	 
	public static Session getSession()
    {
        return theSession;
    }
	
	public GUI getGUI(){
		return gui;
	}
	
	public Keyboard getKeyboard(){
		return keyboard;
	}

    public void run()
	{
		boolean test = true;
		
	    while(test){
			keyboard.setCancelled(false);
			try{
				int input = atm.getCustomerConsole().readMenuChoice("Please choose transaction type", TRANSACTION_TYPES_MENU);
				switch(input){
					case 1: 	System.out.println("withdraw");
								atm.withdraw(user);
								break;
					case 2: 	atm.deposit(user);
								break;
					case 3:		atm.transfer(user);
								break;
					case 4: 	atm.balanceInquiry(user);
								break;
					case 5: 	atm.billPayment(user);
								break;
					case 6:		atm.recentTransactions(user);
								break;
					case 7: 	System.out.println("Dis dont do nuttin");
								break;
					default:	System.out.println("That is not a valid input");
								break;
				}
			}
			catch(CustomerConsole.Cancelled e){
				e.printStackTrace();
			}
		}
    }
    

    public void setPIN(int pin)
    {
        this.pin = pin;
    }
	
	public void display(String text)
    {
        display.display(text);
    }
	
	public void clearDisplay()
    {
        display.clearDisplay();
    }
	
    public String readInput(int mode)
    {
        return keyboard.readInput(mode);
    }
}