/**
 * Created by Darren on 3/30/2016.
 */
public class ChequingAccount extends BankAccount {
    private double withdrawLimit;

    /**
     * Constructor for the ChequingAccount class
     *
     * @param balance
     * @param name
     * @param withdrawLimit
     */
    public ChequingAccount(MoneyType balance, String name, double withdrawLimit) {
        super(balance, name);
        this.withdrawLimit = withdrawLimit;
    }

    /** Withdraw money from the account
     *  @param  amount
     *  @return success of withdrawal
     */
    public boolean withdraw(MoneyType amount) {
        if (sufficientBalance(amount) && withinLimits(amount) && !locked) {
            balance.sub(amount);
            return true;
        }
        return false;
    }

    /**  @override
     *  Overriden toString method
     *  @return formatted ChequingAccount String
     */
    public String toString() {
        return "[Chequing] Account carries a balance of " + balance
                + " with a limit of " + balance.getSymbol() + withdrawLimit;
    }

    /** Compare accounts
     *  NOTE that you can just use == instead
     *  in this case
     *
     *  @param  other account
     *  @return if they are the same
     */
    public boolean equals(BankAccount other){
        return (other instanceof ChequingAccount);
    }

    /** Check if it's within the limits
     *  @param  amount
     *  @return withinLimits or not
     */
    public boolean withinLimits(MoneyType amount) {
        return amount.getAmount() <= withdrawLimit;
    }

    /** Set the withdrawal limit
     *  @param  withdrawLimit
     */
    public void setLimits(double withdrawLimit) {
        this.withdrawLimit = withdrawLimit;
    }
}
