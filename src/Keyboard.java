import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Keyboard extends Panel {
    private static final int IDLE_MODE = 0;
    private static final int PIN_MODE = Session.PIN_MODE;
    private static final int AMOUNT_MODE = Session.AMOUNT_MODE;
    private static final int MENU_MODE = Session.MENU_MODE;
    private static byte currentCurrency = MoneyType.USD;
    private Display display;
    private int mode;
    private StringBuffer currentInput;
    private boolean cancelled;
    private int maxValue;
    private boolean decimal = false;

    public Keyboard(Display display) {
        this.display = display;
        setLayout(new GridLayout(5, 3));

        Button[] digitKey = new Button[10];
        for (int i = 1; i < 10; i++) {
            digitKey[i] = new Button("" + i);
            add(digitKey[i]);
        }

        digitKey[0] = new Button("0");
        add(digitKey[0]);

        Button decimalKey = new Button(".");
        add(decimalKey);

        //add(new Label(""));

        // Create the function keys

        Button currencyKey = new Button("Change Currency");
        currencyKey.setForeground(Color.black);
        currencyKey.setBackground(new Color(128, 128, 255)); // Light blue
        add(currencyKey);

        Button enterKey = new Button("ENTER");
        enterKey.setForeground(Color.black);
        enterKey.setBackground(new Color(128, 128, 255)); // Light blue
        add(enterKey);

        Button clearKey = new Button("CLEAR");
        clearKey.setForeground(Color.black);
        clearKey.setBackground(new Color(255, 128, 128)); // Light red
        add(clearKey);

        Button cancelKey = new Button("CANCEL");
        cancelKey.setBackground(Color.red);
        cancelKey.setForeground(Color.black);
        add(cancelKey);

        for (int i = 0; i < 10; i++)
            digitKey[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    digitKeyPressed(Integer.parseInt(e.getActionCommand()));
                }
            });

        decimalKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                digitKeyPressed(-1);
            }
        });

        currencyKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                currencyKeyPressed();
            }
        });

        enterKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                enterKeyPressed();
            }
        });

        clearKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearKeyPressed();
            }
        });

        cancelKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelKeyPressed();
            }
        });

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                char keyChar = e.getKeyChar();
                int keyCode = e.getKeyCode();
                if (keyChar >= '0' && keyChar <= '9') {
                    digitKeyPressed(keyChar - '0');
                    e.consume();
                } else {
                    switch (keyCode) {
                        case KeyEvent.VK_ENTER:
                            enterKeyPressed();
                            break;
                        case KeyEvent.VK_CLEAR:
                            clearKeyPressed();
                            break;
                        case KeyEvent.VK_CANCEL:

                        case KeyEvent.VK_ESCAPE:
                            cancelKeyPressed();
                            break;
                    }
                    e.consume();
                }
            }
        });

        // Initialize mode and input buffer

        currentInput = new StringBuffer();
        mode = IDLE_MODE;
    }

    public synchronized String readInput(int mode) {
        this.mode = mode;
        currentInput.setLength(0);
        requestFocus();
        try {
            wait();
        } catch (InterruptedException e) {
        }

        this.mode = IDLE_MODE;

        if (cancelled)
            return null;
        else
            return currentInput.toString();
    }

    /**
     * Handle a digit key
     *
     * @param digit the value on the key
     */
    private synchronized void digitKeyPressed(int digit) {
        switch (mode) {
            case IDLE_MODE:
                break;
            case AMOUNT_MODE: {
                if (digit == -1 && !decimal) {
                    currentInput.append(".");
                    decimal = true;
                } else {
                    currentInput.append(digit);
                }
                String input = currentInput.toString();
        /*if (input.length() == 1)
         setEcho("0.0" + input);
         else if (input.length() == 2)
         setEcho("0." + input);
         else*/
                setEcho(symbol() + input/*.substring(0, input.length() - 2) + "." +
         input.substring(input.length() - 2)*/);
                break;
            }
            case MENU_MODE: {
                currentInput.append(digit);
                notify();
            }
        }
    }

    /**
     * Eric's version of currencyKeyPressed
     * has a minor bug when used on the the display balance page
     */

    private synchronized void currencyKeyPressed() {
        switch (mode) {
            case IDLE_MODE:
                break;
            case AMOUNT_MODE:
                if (currentInput.length() > 0) {
                    double test = 0.0;
                    if (currentCurrency == MoneyType.USD) {
                        test = MoneyType.convert(MoneyType.USD, MoneyType.EUR, Double.parseDouble(currentInput.toString()));
                        currentCurrency = MoneyType.EUR;
                    } else if (currentCurrency == MoneyType.EUR) {
                        test = MoneyType.convert(MoneyType.EUR, MoneyType.JPY, Double.parseDouble(currentInput.toString()));
                        currentCurrency = MoneyType.JPY;
                    } else {
                        test = MoneyType.convert(MoneyType.JPY, MoneyType.USD, Double.parseDouble(currentInput.toString()));
                        currentCurrency = MoneyType.USD;
                    }
                    currentInput.setLength(0);
                    currentInput.append(UsefulTools.getRoundedString(test, 2));
                    setEcho(symbol() + currentInput.toString());
                }
                break;
            case MENU_MODE:
                getToolkit().beep();
                break;
        }
    }

    public String symbol() {
        switch (currentCurrency) {
            case MoneyType.USD:
                return "$";
            case MoneyType.EUR:
                return "\u20ac";
            case MoneyType.JPY:
                return "\u00A5";
        }
        return "";
    }

    /**
     * Handle the ENTER key
     */
    private synchronized void enterKeyPressed() {
        switch (mode) {
            case IDLE_MODE:
                notify();
                break;

            case PIN_MODE:
            case AMOUNT_MODE:

                //if (currentInput.length() > 0)
                notify();
                //else
                //    getToolkit().beep();
                break;

            case MENU_MODE:

                getToolkit().beep();
                break;
        }
    }

    /**
     * Handle the CLEAR key
     */
    private synchronized void clearKeyPressed() {
        switch (mode) {
            case IDLE_MODE:
                notify();
                break;

            case AMOUNT_MODE:
                decimal = false;
                currentInput.setLength(0);
                setEcho("0");
                break;

            case MENU_MODE:

                getToolkit().beep();
                break;
        }
    }

    /**
     * Handle the CANCEL KEY
     */
    private synchronized void cancelKeyPressed() {
        switch (mode) {
            case IDLE_MODE:

                // It is possible to press the cancel key when requested
                // to insert an envelope - so notify the envelope acceptor
                // of this fact (notification is ignored if acceptor is
                // not waiting for an envelope)


            case PIN_MODE:
            case AMOUNT_MODE:
            case MENU_MODE:

                cancelled = true;
                notify();
        }
    }

    public synchronized void waitForKeyPress() {
        try {
            wait();
        } catch (InterruptedException e) {
        }
    }

    /**
     * Set the echo string displayed on the display
     *
     * @param echo the text to set the echo to (the whole line)
     */
    private void setEcho(String echo) {
        display.setEcho(echo);
    }

    public void setCancelled(boolean bol) {
        cancelled = bol;
    }
}