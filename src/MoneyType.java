import java.text.DecimalFormat;

public class MoneyType {
    public static final byte USD = 0;
    public static final byte EUR = 1;
    public static final byte JPY = 2;
    //exchange rates
    public static final double EURTOUSD = 1.12;
    public static final double JPYTOUSD = 0.0089;
    //used to determine the currency
    protected byte currency;
    //quantified amount of money
    private double amount;


    /**
     * constructor with currency type defined in parameters
     *
     * @param currency
     */
    public MoneyType(byte currency) {
        this.currency = currency;
        this.amount = 0;
    }

    /**
     * constructor with currency type and money
     * defined in parameters
     *
     * @param currency
     * @param amount
     */
    public MoneyType(byte currency, double amount) {
        this.currency = currency;
        this.amount = amount;
    }

    /**
     * Default constructor set to USD and no money
     */
    public MoneyType() {
        this.currency = USD;
        this.amount = 0;
    }

    /**
     * Convert from one currency to another
     *
     * @param currencyTo   tgt currency
     * @param currencyFrom src currency
     * @param amount       amount in src currency
     * @return returnedAmount    amount in tgt currency
     */
    public static double convert(byte currencyFrom, byte currencyTo, double amount) {
        if (currencyFrom == currencyTo) return amount;

        double returnedAmount = amount;

        //convert all currencies to a common currency -> USD
        if (currencyFrom == EUR)
            returnedAmount *= EURTOUSD;
        else if (currencyFrom == JPY)
            returnedAmount *= JPYTOUSD;

        //convert the currencies to the wanted currency
        //if wanted currency is USD, leave it as is
        if (currencyTo == EUR)
            returnedAmount /= EURTOUSD;
        else if (currencyTo == JPY)
            returnedAmount /= JPYTOUSD;

        return returnedAmount;
    }

    /**
     *	@override toString() method
     * 	returns in format of $x.xx where $ is the currency logo
     */
    public String toString() {
        String amount = new DecimalFormat("#.##").format(this.amount);
        if (!getSymbol().equals("?")) return getSymbol() + amount;
        return "Error in converting money to string";
    }

    /**    Get the symbol of the currency
     *	@return	symbol
     */
    public String getSymbol() {
        if (currency == USD) return "$";
        if (currency == EUR) return "€";
        if (currency == JPY) return "¥";
        return "?";
    }

    /**Change the currency
     * @param	currency	the target currency
     */
    public void changeCurrencyType(byte currency) {
        //if the currencies are the same, do nothing
        if (this.currency == currency) return;

        this.amount = convert(this.currency, currency, this.amount);
        this.currency = currency;
    }

    /**Get the amount of money
     * @return the amount of money
     */
    public double getAmount() {
        return this.amount;
    }

    /**add the money amount of money
     * @param	money	the amount of money wished to be added
     */
    public void add(MoneyType money) {
        this.amount += convert(money.currency, this.currency, money.amount);
    }

    /**subtract the money amount of money
     * @param	money	the amount of money wished to be subtracted
     */
    public void sub(MoneyType money) {
        this.amount -= convert(money.currency, this.currency, money.amount);
    }

    /**
     * Convert currency method for moneytypes
     *
     * @param currencyTo
     * @return
     */
    public double convert(byte currencyTo) {
        return convert(currency, currencyTo, amount);
    }

    /**    Boolean less than equal
     *	@param	other	the other moneytype to be compared to
     *  @return <=
     */
    public boolean le(MoneyType other) {
        return this.amount <= other.convert(currency);
    }

    /**
     * Boolean equal
     *
     * @param other
     * @return ==
     */
    public boolean eq(MoneyType other) {
        return this.amount == other.convert(currency);
    }

    /**
     * Boolean greater than equal
     * @param other
     * @return >=
     */
    public boolean ge(MoneyType other) {
        return this.amount >= other.convert(currency);
    }

    /** Retrieve the currency
     *  @return currency
     */
    public byte getCurrency(){
        return currency;
    }

    /**
     * if the amount is less than equal to 0.001, it is essentially empty
     *
     * @return
     */
    public boolean empty() {
        return this.amount <= 0.001;
    }
}
