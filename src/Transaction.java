public class Transaction {
    public static final byte TRANSFER = 0;
    public static final byte PURCHASE = 1;
    public static final byte REFUND = 2;
    public static final byte DEPOSIT = 3;
    public static final byte WITHDRAW = 4;
    public static final byte PAYMENT = 5;

    private byte transactionType;
    private MoneyType money;
    private BankAccount destinationBankAccount;

    /**
     * Constructor for the transaction class
     *
     * @param destinationBankAccount
     * @param transactionType
     * @param money                  the amount of money moved
     */
    public Transaction(BankAccount destinationBankAccount, byte transactionType, MoneyType money) {
        this.destinationBankAccount = destinationBankAccount;
        this.transactionType = transactionType;
        this.money = money;
    }

    /**
     * @return formatted Transaction string
     * @override
     */
    public String toString() {
        if (transactionType == TRANSFER) return "[Transfer] " + money + " to " + destinationBankAccount.getName();
        if (transactionType == PURCHASE) return "[Purchase] " + money + " at " + destinationBankAccount.getName();
        if (transactionType == REFUND) return "[Refund] " + money + " from " + destinationBankAccount.getName();
        if (transactionType == DEPOSIT) return "[Deposited] " + money + " to " + destinationBankAccount.getName();
        if (transactionType == WITHDRAW) return "[Withdrawal] " + money + " from " + destinationBankAccount.getName();
        if (transactionType == PAYMENT) return "[Payment] " + money + " to " + destinationBankAccount.getName();
        return "[TRANSACTION_ERROR] Unable to read transaction";
    }
}
