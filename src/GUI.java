import java.awt.*;
import java.awt.event.*;

public class GUI extends Panel{
    private CardLayout mainLayout;
    private CardPanel cardPanel;
	private ATMPanel atmPanel;
    
	public GUI(Display display, Keyboard keyboard){
		setBackground(Color.lightGray);
		mainLayout = new CardLayout(5,5);
        setLayout(mainLayout);
		
		atmPanel = new ATMPanel(this, display, keyboard);
		this.add(atmPanel, "ATM");
		
        cardPanel = new CardPanel();
        add(cardPanel, "CARD");
		mainLayout.show(this, "CARD");
		//mainLayout.show(this, "ATM");
	}
	
	public void readCard(){
		mainLayout.show(this, "CARD");
	}
	
	public void showATM(){
		mainLayout.show(this, "ATM");
	}
	
	public static GridBagConstraints makeConstraints(int row, int col, int width, int height, int fill){ 
        GridBagConstraints g = new GridBagConstraints();
        g.gridy = row;
        g.gridx = col;
        g.gridheight = height;
        g.gridwidth = width;
        g.fill = fill;
        g.insets = new Insets(2,2,2,2);
        g.weightx = 1;
        g.weighty = 1;
        g.anchor = GridBagConstraints.CENTER;
        return g;
    }
}