import java.text.DecimalFormat;

/**
 * Bunch of tools with pretty much no cohesiveness, but useful to have
 */
public class UsefulTools{
	/**
	 * Rounding function, maybe not even used in the future
	 *
	 * @param number
	 * @param decimals
	 * @return
	 */
	public static double round(double number,int decimals){
		return Math.round(number * Math.pow(10,decimals))/Math.pow(10,decimals);
	}

	public static String getRoundedString(double number, int decimals) {
		if (decimals < 1) return "" + round(number, 0);
		String stringFormat = "#.";
		for (int i = 0; i < decimals; ++i, stringFormat += "#") ;
		return new DecimalFormat(stringFormat).format(round(number, decimals));
	}
}