CPS406 Tester input

3/24/16

NOTE :Only reason it is so long is because I have also included the console output and my inputs. Just so you guys follow what I was doing. If you guys don't need it, just tell me and I'll exclude it next time.

Run 1

-Tried to withdraw 200 from chequing and said " Failed to withdraw" however did not state why. I know its obvious that the likely situation is " not enough in account" but I am not sure on how accurate you guys are coding this to a real atm. Reasons like " no more money in atm" or " failed connection from atm to bank" is also valid irl. Source: I have faced this problem before

-Deposited 200 to chequing and was immediately sent back to " Choose transaction type" page. Did not mention whether deposit was successful compared to withdraw where it told me I failed once I entered a number.

-Similar situation in transferring from chequing to savings. Transferred 200 but did not say whether it was successful or not. Just brought me back to " choose trans" page


-Arrived to "Recent transactions" page and was shown this output:

Recent Transactions:

1 [Transfer] $150 to null

2 [Transfer] $200 to null

3 [Deposited] $200 to null

Should null be either chequing or savings? 

Console from my POV of this run :

Welcome to our ATM

If this were a proper ATM then we would ask for you to insert your card.

However, for this simulation we ask that you enter your card Number.

(Again since this is not a real ATM simply enter a random sequence of 12 digits.)

Enter a number: 123456789101112

Enter your 4 digit pin: 4171

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

1

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Enter the amount of cash that you would like to withdraw

 200
 
Failed to withdraw

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

2

Please select an account

1) To Chequing

2) To Savings

3) Cancel

1

Enter the amount of cash that you would like to deposit

200

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

3

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Please select an account

1) To Chequing

2) To Savings

3) Cancel

2

Enter the amount that you would like to transfer

200

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Your account balance is: $0

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

Your account balance is: $200

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

3

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

Please select an account

1) To Chequing

2) To Savings

3) Cancel

1

Enter the amount that you would like to transfer

150

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Your account balance is: $150

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

1) Payee Company 001	$310.66

2) Cancel

2
Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

1) Payee Company 001	$310.66

2) Cancel

2

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

6

Recent Transactions:

1 [Transfer] $150 to null

2 [Transfer] $200 to null

3 [Deposited] $200 to null

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

7
Thank you for using our ATM

Run 2

-Told me to enter a random sequence of 12 digits, only entered  6 but was still allowed to go through. 

-Asked to enter a 4 digit pin, only put 2 but was still allowed to go through.

-Tried out bill payment and entered exact amount to pay . After, was brought back to “ choose trans” page. Did not tell me whether the bill payment was a success or not.

-Tried out bill payment again and paid over amount required and was brought back to “ choose trans” page. Did not tell me whether or not it was a successful payment. NOTE: Should there be a restriction on how much I can pay? Like over the amount required? Just wondering.

-Checked bill payment again to see if the bills I paid for are gone. But still there.

-Went to bill payment again but instead of chequing went to savings and tried to pay off amount.  Brought me back to “ choose trans” page after I finished. Did not tell me whether or not it was a success. Shouldn’t be since savings balance is $0.

-Checked Recent Transactions , this is output:

Recent Transactions:

1 [Payment] $200 to null

2 [Payment] $400000 to null

3 [Payment] $231.18 to null

4 [Deposited] $1000000000000000 to null 

Guessing null here should be the companies, savings, or chequing.


Console from my POV of this run :

Welcome to our ATM

If this were a proper ATM then we would ask for you to insert your card.

However, for this simulation we ask that you enter your card Number.

(Again since this is not a real ATM simply enter a random sequence of 12 digits.)

Enter a number: 111111

Enter your 4 digit pin: 12

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

2

Please select an account

1) To Chequing

2) To Savings

3) Cancel

1

Enter the amount of cash that you would like to deposit

1000000000000000

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Your account balance is: $1000000000000000

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

1) Payee Company 001	$231.18

2) Payee Company 002	$342.79

3) Cancel

1

How much do you want to pay

231.18

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Your account balance is: $999999999999768.8

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

1) Payee Company 001	$231.18

2) Payee Company 002	$342.79

3) Cancel

2
How much do you want to pay

400000

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

1) Payee Company 001	$231.18

2) Payee Company 002	$342.79

3) Cancel

3

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

5

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

1) Payee Company 001	$231.18

2) Payee Company 002	$342.79

3) Cancel

1

How much do you want to pay

200

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

4

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

Your account balance is: $0

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

6

Recent Transactions:

1 [Payment] $200 to null

2 [Payment] $400000 to null

3 [Payment] $231.18 to null

4 [Deposited] $1000000000000000 to null

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

1

Please select an account

1) From Chequing

2) From Savings

3) Cancel

1

Enter the amount of cash that you would like to withdraw

200

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

1

Please select an account

1) From Chequing

2) From Savings

3) Cancel

2

Enter the amount of cash that you would like to withdraw

200

Failed to withdraw

Please choose transaction type:

1) Withdraw

2) Deposit

3) Transfer

4) Balance Inquiry

5) Bill Payment

6) Recent Transactions

7) Exit

7

Thank you for using our ATM
